package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            Collections.sort(inputNumbers);
            int lines;
            int columns;
            int elemSum = 1;
            int count = 0;
            for (int i = 2; elemSum < inputNumbers.size(); i++) {
                elemSum = i + elemSum;
                count++;
            }
            lines = count + 1;
            columns = ((lines * 2) - 1);

            int center = columns / 2;
            int ind = 0;
            int numbInLine = 1;
            int[][] matrix = new int[lines][columns];
            for (int i = 0, zer = 0; i < lines; i++, zer++, numbInLine++) {
                for (int j = 0; j < numbInLine * 2; j += 2) {
                    int x = center - zer;
                    matrix[i][j + x] = inputNumbers.get(ind++);
                }
            }
            return matrix;
        } catch (Exception | OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
    }
}

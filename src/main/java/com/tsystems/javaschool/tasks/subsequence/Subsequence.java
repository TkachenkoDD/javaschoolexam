package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        try {
            List listOfMatches = new ArrayList();
            boolean b;

            for (int i = 0; i < y.size(); i++) {
                b = x.contains(y.get(i));
                if (b)
                    listOfMatches.add(y.get(i));
            }

            for (int i = 0; i < listOfMatches.size(); i++) {
                if (listOfMatches.get(i) != x.get(i))
                    listOfMatches.remove(i);
            }

            return x.equals(listOfMatches);

        } catch (NullPointerException e) {
            throw new IllegalArgumentException();
        }
    }
}

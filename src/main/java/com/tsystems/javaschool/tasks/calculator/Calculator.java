package com.tsystems.javaschool.tasks.calculator;

import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
            String input = sequenceOfParentheses(statement);
            String expression = getExpression(input);
            return getAnswer(calculations(expression));
        } catch (NullPointerException | EmptyStackException | NumberFormatException e) {
            return null;
        }
    }

    private String getExpression(String statement) {
        StringBuilder current = new StringBuilder();
        Stack<Character> stack = new Stack<>();

        int prior;
        for (int i = 0; i < statement.length(); i++) {
            prior = priority(statement.charAt(i));
            if (prior == 0) current.append(statement.charAt(i));
            if (prior == 1) stack.push(statement.charAt(i));
            if (prior > 1) {
                current.append(' ');

                while (!stack.empty()) {
                    if (priority(stack.peek()) >= prior) current.append(stack.pop());
                    else break;
                }
                stack.push(statement.charAt(i));
            }
            if (prior == -1) {
                current.append(' ');
                while (priority(stack.peek()) != 1) current.append(stack.pop());
                stack.pop();
            }

        }
        while (!stack.empty()) current.append(stack.pop());

        return current.toString();
    }

    private double calculations(String exp) {
        StringBuilder operand = new StringBuilder();
        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < exp.length(); i++) {
            if (exp.charAt(i) == ' ') continue;

            if (priority(exp.charAt(i)) == 0) {
                while (exp.charAt(i) != ' ' && priority(exp.charAt(i)) == 0) {
                    operand.append(exp.charAt(i++));
                    if (i == exp.length()) break;
                }

                stack.push(Double.parseDouble(operand.toString()));
                operand = new StringBuilder();
            }

            if (priority(exp.charAt(i)) > 1) {
                double a = stack.pop(), b = stack.pop();

                if (exp.charAt(i) == '+') stack.push(b + a);
                if (exp.charAt(i) == '-') stack.push(b - a);
                if (exp.charAt(i) == '*') stack.push(b * a);
                if (exp.charAt(i) == '/' && a != 0) stack.push(b / a);
            }
        }
        return stack.pop();
    }

    private String getAnswer(double x) {
        String formattedDouble = String.format("%.4f", x).replace(',', '.');

        if (x % 1 == 0) {
            int y = (int) x;
            return String.valueOf(y);

        } else if (String.valueOf(x).length() <= 6) {
            return String.valueOf(x);

        } else return formattedDouble;
    }

    private int priority(char c) {
        if (c == '*' || c == '/') return 3;
        else if (c == '+' || c == '-') return 2;
        else if (c == '(') return 1;
        else if (c == ')') return -1;
        else return 0;
    }

    private String sequenceOfParentheses(String str){
     int x = 0, y = 0;
     for (int i = 0; i< str.length(); i++){
         if (str.charAt(i) == '(') x++;
         if (str.charAt(i) == ')') y++;
     }
     if (x != y)
        return null;
        return str;
    }
}
